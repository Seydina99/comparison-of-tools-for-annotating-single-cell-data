<img src="IMG/logo.png" align="center"  />
<h1> Comparison of tools for annotating Single Cell data </h1>

## Objective
In my internship, I used CellO and scMRMA which are annotation tools released in 2021 that use different methods to predict cell types. The objective of my internship was to find comprehensive and relevant methods to compare these annotation tools which is a subject never done. The main challenge for me were to understand how the tools work, and run them. I’ve tackled this challenge by using their articles and documentation extensively to develop a reusable comparative pipeline. To this end, I use an annotated dataset obtained by Fluorescence-Activated Cell Sorting technology to compare these annotation tools results to them. In this work, I use an annotated single-cell dataset as ground truth, and I propose two metrics to compare the consistency and the accuracy of the predictions of the methods with regard to the ground truth label. These metrics are intuitive and take into account the hierarchical structure of the Cell Ontology. I have developed an automatic pipeline to conduct this comparative analysis automatically, and to generate figures of the results.
Finally, I propose a proof of concept of my comparative pipeline, and I show its re-usability with different modalities (e.g. sequencing technology of the dataset).

## Pipeline
<table align="center"><tr><td align="center" width="9999">
<img src="IMG/Pipeline.png" align="center" width="600"/>
</table>

## Methods
- <a href="https://github.com/deweylab/CellO"> CellO </a>
<table align="center"><tr><td align="center" width="9999">
<img src="IMG/CellO.png" align="center" width="600"/>
</table>

- <a href="https://github.com/JiaLiVUMC/scMRMA"> scMRMA </a>
<table align="center"><tr><td align="center" width="9999">
<img src="IMG/scMRMA.png" align="center" width="600"/>
</table>

## Results
- Consistency
<table align="center"><tr><td align="center" width="9999">
<img src="Results/Consistency.png" align="center" width="600"/>
</table>

- Accuracy
<table align="center"><tr><td align="center" width="9999">
<img src="Results/Accuracy.png" align="center" width="600"/>
</table>

## Author
<p>DIOUF Seydina Mouhamed</p>
<p>Master's degree in Bioinformatics at Clermont Auvergne University</p>

## Contact  
- email: seydinamouhameddiouf808@gmail.com  
- LinkedIn : https://www.linkedin.com/in/seydina-mouhamed-diouf-50348a1ab

## Supervisors
<p>Patricia THEBAULT | Professor at the University of Bordeaux</p>
<p>Yanis ASLOUDJ | Phd student at LaBRI</p>
<p>Francois ENAULT | Professor at Clermont Auvergne University</p>

<table align="center"><tr><td align="center" width="9999">
<img src="IMG/logo.png" align="center"  />
</table>
