"""----------CELLO----------"""
import subprocess
import sys
print(sys.version)
print(sys.version_info)

print()
print("-----------------------------------")
print("All packages are well installed  :)")
print("-----------------------------------")
print()

###_____Download a single-cell RNA lung tissue dataset
"""
GEO_DATASET_URL = 'ftp://ftp.ncbi.nlm.nih.gov/geo/samples/GSM3516nnn/GSM3516673/suppl/GSM3516673_MSK_LX682_NORMAL_dense.csv.gz'
subprocess.run('curl {} > GSM3516666_MSK_LX682_NORMAL_dense.csv.gz'.format(GEO_DATASET_URL), shell=True)
"""
print()
print("--------------------------------")
print("Single-data is well download  :)")
print("--------------------------------")
print()
#####_____Import packages

import os
import pandas as pd
import numpy as np
import matplotlib
import spicy
import scanpy as sc
from anndata import AnnData
import cello
import gunzip 

print()
print("--------------------------------------------")
print("All necessary packages are well imported  :)")
print("--------------------------------------------")
print()
#####_____Load the expression matrix using Pandas and Scanpy
#subprocess.run("gunzip GSM3516666_MSK_LX682_NORMAL_dense.csv.gz", shell=True)
os.listdir()
df = pd.read_csv('GSM3516673_MSK_LX682_NORMAL_dense.csv', index_col=0)
adata = AnnData(df)
#df

#####_____Variant 2 for normalizing and clustering
# Normalize the data by estimating log transcripts per million (TPM)

sc.pp.normalize_total(adata, target_sum=1e6)
sc.pp.log1p(adata)

print( "=> Data are well normalized")

# Select most highly variable genes

sc.pp.highly_variable_genes(adata, n_top_genes=10000)

print( "=> Most highly variable genes were selected")

# Perform principal components analysis (PCA)

sc.pp.pca(adata, n_comps=50)

print( "=> PCA is done !")

# Compute nearest-neighbors graph

sc.pp.neighbors(adata, n_neighbors=15)

# Cluster the cells with Leiden

sc.tl.leiden(adata, resolution=2.0)

print( "=> Cells are now clustered with Leiden")


##### Download CellO ressources
"""
subprocess.run("wget http://deweylab.biostat.wisc.edu/cell_type_classification/resources_v2.0.0.tar.gz > resources_v2.0.0.tar.gz", shell=True)
subprocess.run("tar -zxf resources_v2.0.0.tar.gz", shell=True)
subprocess.run("rm resources_v2.0.0.tar.gz", shell=True)
"""

##### Download the trained-model
"""
subprocess.run("python3 CellO-master/cello/cello_train_model.py", shell=True)
"""

#####_____Specify CellO’s resource location

cello_resource_loc = os.getcwd()


#####_____Variant 1 for running CellO
"""Because CellO's pre-trained models expect different genes than those in the current expression matrix, we will need to train a new CellO classifier using CellO's built-in training set."""

model_prefix = "GSM3516666_LX682_NORMAL" # <-- The trained model will be stored in a file called GSM3516666_LX682_NORMAL.model.dill

cello.scanpy_cello(
    adata,
    'leiden',
    cello_resource_loc,
    out_prefix=model_prefix
)


#####_____Variant 2 for running CellO
"""If you have a pre-trained model that is compatible with the genes in the target expression matrix, then we can run CellO without training a new model. For example, once we run the first variant of Step 7 (above), we don't need to run it again. We simply use the trained model from that step.

model_prefix = "GSM3516666_LX682_NORMAL"

cello.scanpy_cello(
    adata,
    'leiden',
    cello_resource_loc,
    model_file=f'{model_prefix}.model.dill'
)
"""
#####_____Run UMAP

sc.tl.umap(adata)

print()
print("-----------------")
print("Umap run well  :)")
print("-----------------")
print()

#####_____Create and saveUMAP plot with cells colored by cluster

fig = sc.pl.umap(adata, color='leiden', return_fig=True)
out_file = 'clusters_under_clustered.pdf' # <-- Name of the output file
fig.savefig(out_file, bbox_inches='tight', format='pdf')
print()
print( "=> Plot with cells colored by cluster is saved")


#####_____Create UMAP plot with cells colored by most-specific predicted cell type

fig = sc.pl.umap(adata, color='Most specific cell type', return_fig=True)
out_file = 'specific_cell_types.pdf' # <-- Name of the output file
fig.savefig(out_file, bbox_inches='tight', format='pdf')
print( "=> UMAP plot with cells colored by most-specific predicted cell type")

#####_____Create UMAP plot with cells colored according their probability of being T cells

fig = sc.pl.umap(adata, color='T cell (probability)', vmin=0.0, vmax=1.0, return_fig=True)
out_file = 'T_cell_probability.pdf' # <-- Name of the output file
fig.savefig(out_file, bbox_inches='tight', format='pdf')
print( "=> UMAP plot with cells colored according their probability of being T cells")
#####_____Create UMAP plot with cells colored according whether they are classified as being T cells

fig = sc.pl.umap(adata, color='T cell (binary)', return_fig=True)
out_file = 'T_cell_binary.pdf' # <-- Name of the output file
fig.savefig(out_file, bbox_inches='tight', format='pdf')
print( "=> UMAP plot with cells colored according whether they are classified as being T cells")
#####_____Visualize cell type probabilities assigned to a specific cluster overlaid on the Cell Ontology graph

fig, ax = cello.cello_probs(adata, '21', cello_resource_loc, 0.5, clust_key='leiden');
out_file = 'probs_on_graph.png' # <--- Name of the output file
fig.savefig(out_file, format='png', dpi=300)
print( "=> Visualize cell type probabilities assigned to a specific cluster overlaid on the Cell Ontology graph")
#####_____Write CellO's output to a TSV file

cello.write_to_tsv(adata, 'GSM3516673_MSK_LX682_NORMAL.CellO_output.tsv')

print("------------------------------------------------------------------------------------------------------------------")

"""----------ONCLASS----------"""

from anndata import read_h5ad
from scipy import stats, sparse
import numpy as np
import sys
from collections import Counter
from OnClass.OnClassModel import OnClassModel
from utils import read_ontology_file, read_data, run_scanorama_multiply_datasets
from config import ontology_data_dir, scrna_data_dir, model_dir, Run_scanorama_batch_correction, NHIDDEN, MAX_ITER
train_file = scrna_data_dir + '/Lemur/microcebusBernard.h5ad'
test_file = scrna_data_dir + '/Lemur/microcebusAntoine.h5ad'

train_label = 'cell_ontology_id'
test_label = 'cell_ontology_id'
model_path = model_dir + 'example_file_model'

# read data
print ('read ontology data and initialize training model...')
cell_type_nlp_emb_file, cell_type_network_file, cl_obo_file = read_ontology_file('cell ontology', ontology_data_dir)
OnClass_train_obj = OnClassModel(cell_type_nlp_emb_file = cell_type_nlp_emb_file, cell_type_network_file = cell_type_network_file)

print ('read training single cell data...')
train_feature, train_genes, train_label, _, _ = read_data(train_file, cell_ontology_ids = OnClass_train_obj.cell_ontology_ids,
	exclude_non_leaf_ontology = False, tissue_key = 'tissue', AnnData_label_key = train_label, filter_key = {},
	nlp_mapping = False, cl_obo_file = cl_obo_file, cell_ontology_file = cell_type_network_file, co2emb = OnClass_train_obj.co2vec_nlp)
#you can also replace it with your own data and make sure that:
#train_feature is a ncell by ngene matrix
#train_genes is a ngene long vector of gene names
#train_label is a ncell long vector
print ('embed cell types using the cell ontology...')
OnClass_train_obj.EmbedCellTypes(train_label)

print ('read test single cell data...')
x = read_h5ad(test_file)
test_label = x.obs[test_label].tolist()
test_feature = x.X.toarray()
test_genes = np.array([x.upper() for x in x.var.index])

# train the model
if Run_scanorama_batch_correction:
	train_feature, test_feature = run_scanorama_multiply_datasets([train_feature, test_feature], [train_genes, test_genes], scan_dim = 10)[1]
	print (np.shape(train_feature), np.shape(test_feature))

print ('generate pretrain model. Save the model to $model_path...')
cor_train_feature, cor_test_feature, cor_train_genes, cor_test_genes = OnClass_train_obj.ProcessTrainFeature(train_feature, train_label, train_genes, test_feature = test_feature, test_genes = test_genes)
OnClass_train_obj.BuildModel(ngene = len(cor_train_genes), nhidden = NHIDDEN)
OnClass_train_obj.Train(cor_train_feature, train_label, save_model = model_path, max_iter = MAX_ITER)

print ('initialize test model. Load the model from $model_path...')
OnClass_test_obj = OnClassModel(cell_type_nlp_emb_file = cell_type_nlp_emb_file, cell_type_network_file = cell_type_network_file)
cor_test_feature = OnClass_train_obj.ProcessTestFeature(cor_test_feature, cor_test_genes, use_pretrain = model_path, log_transform = False)
OnClass_test_obj.BuildModel(ngene = None, use_pretrain = model_path)
#prediction

pred_Y_seen, pred_Y_all, pred_label = OnClass_test_obj.Predict(cor_test_feature, test_genes = cor_test_genes, use_normalize=False)
pred_label_str = [OnClass_test_obj.i2co[l] for l in pred_label]
#x.obs['OnClass_annotation_tree_based_ontology_ID'] = pred_label_str
#x.write(scrna_data_dir + 'Pilot12.annotated.h5ad')
