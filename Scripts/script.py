print("----------------------------------------------------------------------------------------------------------------------------------------------------------------------")
print("|     RUNNING ANNOTATION TOOLS     |")
print("------------------------------------", "\n", "\n")

# Import necessary packages
print("-----> Import necessary packages")
import subprocess
import os
import pandas as pd
from collections import defaultdict
import matplotlib.pyplot as plt
#subprocess.run("pip install -U kaleido", shell=True)
import kaleido
import statistics


print("_________________________")
print("|         CELLO         |")
print("°°°°°°°°°°°°°°°°°°°°°°°°°")
print("  °°°°°°°°°°°°°°°°°°°°°")
print("    °°°°°°°°°°°°°°°°°")
print("      °°°°°°°°°°°°°")
print("        °°°°°°°°°")
print("          °°°°°")
print("            °")

# Change directory
os.chdir("CellO/")

# Load files 
subprocess.run("cp ../config_files.py ./ ", shell=True)
"""
# Run CellO
subprocess.run("python3 CellO.py", shell=True)
"""
# remove config_files.py
subprocess.run("rm config_files.py ", shell=True)

# Reading the output file
print("-----> Reading the output file")
id_list_CellO = []
terms_list_CellO = []
with open("CellO_output.tsv", "r") as f1:
	for li in f1:
		li = li.rstrip()
		if len(li.split()) < 2 :
			Message = "not interesting"
		else:
			id = li.split()[0]
			CL = li.split("	")[-1]
		id_list_CellO.append(id)
		terms_list_CellO.append(CL)
		
# Remove the first line of the lists (early ... Most specific cell type)
print("-----> Remove the first line of the lists (early ... Most specific cell type")
id_list_CellO = id_list_CellO[1:]
terms_list_CellO = terms_list_CellO[1:]

# Checking if there is no problem with output file
print("-----> Checking if there is no problem with output file", "\n")

if len(terms_list_CellO) == len(id_list_CellO) :		
	print("Number of ontology terms : ", len(id_list_CellO))
	print("--------------------------")
else:
	print("Error in output file : len(id_list_CellO) != len(terms_list_CellO)")

# Creating a dataframe with ids and terms	
df_CellO = pd.DataFrame(list(zip(id_list_CellO, terms_list_CellO)), columns = ['ID','CellO'])	
print(df_CellO)

print("_________________________")
print("|        OnClass        |")
print("°°°°°°°°°°°°°°°°°°°°°°°°°")
print("  °°°°°°°°°°°°°°°°°°°°°")
print("    °°°°°°°°°°°°°°°°°")
print("      °°°°°°°°°°°°°")
print("        °°°°°°°°°")
print("          °°°°°")
print("            °")


"""
# Change directory
os.chdir("../OnClass/")

# Load files 
subprocess.run("cp ../config_files.py ./", shell=True)

# Run OnClass
subprocess.run("python3 OnClass.py", shell=True)

# Remove config_files.py
subprocess.run("rm config_files.py ", shell=True)
"""





print("_________________________")
print("|        scMRMA         |")
print("°°°°°°°°°°°°°°°°°°°°°°°°°")
print("  °°°°°°°°°°°°°°°°°°°°°")
print("    °°°°°°°°°°°°°°°°°")
print("      °°°°°°°°°°°°°")
print("        °°°°°°°°°")
print("          °°°°°")
print("            °")


os.chdir("../scMRMA/")

# Load files 
subprocess.run("cp ../config_files.py ./", shell=True)
"""
subprocess.run("r scMRMA.R", shell=True)
"""

# Reading the output file
print("-----> Reading the output file")
id_list_scMRMA = []
terms_list_scMRMA = []
with open('output.csv', 'r') as f3:
	for li in f3:
		li = li.rstrip()
		CL = ""
		if len(li.split(",")) < 2:
			Message = "not interesting"
		else:
			id = li.split(",")[0]
			trajectory = li.split(",")[1:]
			CL = trajectory[-1]
			
		id_list_scMRMA.append(id)
		terms_list_scMRMA.append(CL)
		
# Remove the first line of the lists (Level1 ...  Level4)
print("-----> Remove the first line of the lists (Level1 ...  Level4)", "\n")
id_list_scMRMA = id_list_scMRMA[1:]
terms_list_scMRMA = terms_list_scMRMA[1:]

# Checking if there is no problem with output.csv
if len(terms_list_scMRMA) == len(id_list_scMRMA) :		
	print("Number of ontology terms : ", len(id_list_scMRMA))
	print("--------------------------")			
else:
	print("Error in output file : len(id_list_scMRMA) != len(terms_list_scMRMA)")		

# Creating a dataframe with ids and terms

df_scMRMA = pd.DataFrame(list(zip(id_list_scMRMA, terms_list_scMRMA)), index = range(len(id_list_scMRMA)), columns = ['ID', 'scMRMA'])

print(df_scMRMA)


print("_________________________")
print("|       CELL TYPES       |")
print("°°°°°°°°°°°°°°°°°°°°°°°°°")
print("  °°°°°°°°°°°°°°°°°°°°°")
print("    °°°°°°°°°°°°°°°°°")
print("      °°°°°°°°°°°°°")
print("        °°°°°°°°°")
print("          °°°°°")
print("            °")


os.chdir("../DATA/")

# Reading the cell types file
print("-----> Reading the output file")

dict_cell_types = defaultdict(str)
with open("cell_types.csv", "r") as f4:
	for li in f4:
		li = li.rstrip().split(",")[:-1]
		if len(li) == 2 and li[0] != "0":    # Filter the first line of the dataset
			id = li[0]
			CL = li[1]
			dict_cell_types[id] = CL
		elif len(li) == 3:
			id = li[0]
			CL = li[1] + "," + li[2]
			dict_cell_types[id] = CL
		elif len(li) == 4: 
			print("ERROR ---> len(li) > 3", "\n", "You have to put another threshold ! ", "\n", li)

# Overlapping datase's Ids to cell types 
id_list_CT = []
terms_list_CT = []
for id in id_list_CellO:
	if id in dict_cell_types:
		CL = dict_cell_types[id]
	id_list_CT.append(id)
	terms_list_CT.append(CL)

# Checking if there is no problem with output.csv
print("-----> Checking if there is no problem with output file", "\n")

if len(terms_list_CT) == len(id_list_scMRMA) :		
	print("Number of Cell types terms : ", len(id_list_scMRMA))
	print("--------------------------")			
else:
	print("Error in output file : len(id_list_scMRMA) != len(terms_list_scMRMA)")
	
# Creating a dataframe with ids and terms

df_CT = pd.DataFrame(list(zip(id_list_CT, terms_list_CT)), index = range(len(id_list_CT)), columns = ['ID', 'CELL TYPES'])

print(df_CT, "\n")
	

			
print("----------------------------------------------------------------------------------------------------------------------------------------------------------------------")
print("|     VISUALIZATION OF RESULTS WITH ONTOLOGY TERMS     |")
print("--------------------------------------------------------", "\n")

# Join DataFrames to create a new DataFrame (ID by specific tool prediction)
print("Global DataFrame including all tools")
print("____________________________________", "\n")

df = pd.merge(df_CellO, df_scMRMA, how = "outer", on = ["ID"])
df = pd.merge(df, df_CT, how = "outer", on = ["ID"])

print(df)

# Determine unpredicted cells
print("Unpredicted cells ")
print("__________________", "\n")


TOOLS = ["CellO", "scMRMA"]
for tool in TOOLS:
	cell_types_predicted = df[tool].tolist()
	unpredicted_cells = 0
	for cell in cell_types_predicted:
		if str(cell) == "nan" or str(cell) == "NaN":
			unpredicted_cells += 1
			
	print(tool, " : ", unpredicted_cells)
	
print("----------------------------------------------------------------------------------------------------------------------------------------------------------------------")
print("|     ONTOLOGY DATA     |")
print("-------------------------", "\n", "\n")

# Import necessary packages
print("=> Import necessary packages")
import networkx as nx
from nxontology.imports import from_file

# LOAD ONLOLOGY REFERENCES
print("=> LOAD ONLOLOGY REFERENCES", "\n")
os.chdir("../utils/")

def load_ontology(path: str) -> nx.DiGraph:
    """Load an .obo ontology with "is_a" relationships and non-obsolete nodes only.
    The edges are directed from the children terms to their parents."""
    ontology = from_file(path)
    return ontology.graph.reverse()
    
graph = load_ontology("cell_types.obo")

def get_cell_id_wrt_label(graph: nx.DiGraph) -> dict[str, str]:
    """Get a dict with cell labels as keys (e.g. "macrophage") and their unique CL id as values ("CL:0000235")."""
    cell_id_wrt_label = {}
    for id, data in graph.nodes.items():
        cell_id_wrt_label[data["name"]] = id
    return cell_id_wrt_label
    
cell_id_wrt_label = get_cell_id_wrt_label(graph)  


# Changing terms to CL
print("=> Changing terms to CL")

CL_list_CellO = []
for term in terms_list_CellO:
	if term in cell_id_wrt_label:
		CL = cell_id_wrt_label[term]
	CL_list_CellO.append(CL)
	
CL_list_scMRMA = []
for term in terms_list_scMRMA:
	if term in cell_id_wrt_label:
		CL = cell_id_wrt_label[term]
	CL_list_scMRMA.append(CL)

CL_list_CT = []
for term in terms_list_CT:
	if term in cell_id_wrt_label:
		CL = cell_id_wrt_label[term]
	CL_list_CT.append(CL)

#for i in range(len(terms_list_CellO)):
#	print(CL_list_CellO[i], " ----- ", CL_list_scMRMA[i], " ----- ", CL_list_CT[i])	
	
	
# Creating dataframes with ids and CL
print("=> Creating dataframes with ids and CL")

df2_CellO = pd.DataFrame(list(zip(id_list_CellO, CL_list_CellO)), index = range(len(id_list_CellO)), columns = ['ID', 'CELLO'])
df2_scMRMA = pd.DataFrame(list(zip(id_list_scMRMA, CL_list_scMRMA)), index = range(len(id_list_scMRMA)), columns = ['ID', 'scMRMA'])
df2_CT = pd.DataFrame(list(zip(id_list_CT, CL_list_CT)), index = range(len(id_list_CT)), columns = ['ID', 'CELL TYPES'])		

# Join DataFrames to create a new DataFrame (ID by CL)
print("=> Join DataFrames to create a new DataFrame (ID by CL)", "\n")

print(" Global DataFrame including all tools")
print("______________________________________", "\n")

df2 = pd.merge(df2_CellO, df2_scMRMA, how = "outer", on = ["ID"])
df2 = pd.merge(df2, df2_CT, how = "outer", on = ["ID"])	

print(df2)

print("----------------------------------------------------------------------------------------------------------------------------------------------------------------------")
print("|     COMPARISON BETWEEN THESE TOOLS AND REAL ANNOTATION     |")
print("--------------------------------------------------------------", "\n")

print(">>>>>>>>>>>>>>>>>>>>>>>")
print("|     FIRST METHOD    |")
print(">>>>>>>>>>>>>>>>>>>>>>>", "\n")

print(" Do these tools predict the good ontology (CL...) ?", "\n", "Here, we will just verify if the CL terms are the same or not that those in the annotated cell types  and calculate metrics.", "\n")

list_tools = ["CellO", "scMRMA"]
score_CellO = 0
score_scMRMA = 0
for i in range (len(CL_list_CT)):
	good_CL = CL_list_CT[i]
	CellO_CL = CL_list_CellO[i]
	scMRMA_CL = CL_list_scMRMA[i]
	if CellO_CL == good_CL:
		score_CellO += 1
	if scMRMA_CL == good_CL:
		score_scMRMA += 1
score = {"CellO": score_CellO, "scMRMA": score_scMRMA}
print(" Results")
print("_________", "\n")

print("Score CellO :", score_CellO, "/", len(CL_list_CT), "     => ", round(((score_CellO/(len(CL_list_CT)))*100), 2), "%")
print("Score scMRMA :", score_scMRMA, "/", len(CL_list_CT), "     => ", round(((score_scMRMA/(len(CL_list_CT)))*100), 2), "%", "\n")



print(">>>>>>>>>>>>>>>>>>>>>>>>")
print("|     SECOND METHOD    |")
print(">>>>>>>>>>>>>>>>>>>>>>>>", "\n")

print(" Hypothesis : The CL predicted by theses tools are more specific than the annotated cells types.", "\n", "If this hypothesis is true, the predicted CL will be in the list of all cells types CL which have as parent the annotated cells type CL.", "\n")


TOOLS = {
"CellO": CL_list_CellO,
"scMRMA": CL_list_scMRMA
}
nb_CL = len(CL_list_CellO)
dict_comp_by_tool = defaultdict(defaultdict)
consistency_metric = defaultdict(int)
Dmin_CellO = []
Dmin_scMRMA = []
accuracy = 0
accuracy_CellO = 0
accuracy_scMRMA = 0
for tool in TOOLS: 
	CL_list = TOOLS[tool]
	MS = -score[tool]
	LS = -score[tool]
	consistency = score[tool]
	D_list = []
	for i in range (len(CL_list_CT)):
		parent_node = CL_list_CT[i]
		CL_predicted = CL_list[i]
		Dmin = 0
		# Do a tool find CL terms more specific that the other tool can't find ?
		objective = "Most_specific"
		def get_subgraph_from_root(graph: nx.DiGraph, root_id: str) -> nx.DiGraph:

			"""From a broad root term, generate a subgraph containing all the children terms.""" 
			node_ids = nx.ancestors(graph, root_id)
			node_ids.add(root_id)
			return nx.subgraph(graph, node_ids)
			
		subgraph_from_root = get_subgraph_from_root(graph, parent_node)
		
			
		def get_cell_id_wrt_label(graph: nx.DiGraph) -> dict[str, str]:  
			"""Get a dict with cell labels as keys (e.g. "macrophage") and their unique CL id as values ("CL:0000235").""" 
			cell_id_wrt_label = {}
			for id, data in graph.nodes.items():
				cell_id_wrt_label[data["name"]] = id
			return cell_id_wrt_label
	    
		cell_id_wrt_label = get_cell_id_wrt_label(subgraph_from_root) 
		
		children_list = []
		for children_term in cell_id_wrt_label:
			children_list.append(cell_id_wrt_label[children_term])
			
		if CL_predicted in children_list:
			MS += 1
			CL_list[i] = parent_node
		
		# Do a tool find CL terms less specific that the other tool can't find ?
		objective = "Less_specific"
		def get_subgraph_from_leaves(
			graph: nx.DiGraph, leaf_ids: list[str] | set[str]
		) -> nx.DiGraph:
			"""Generate a subgraph encompassing all the input node ids and their parents from a networkx Graph."""
			node_ids = set()
			for lid in leaf_ids:
				node_ids.update(nx.descendants(graph, lid))
				node_ids.add(lid)
			return nx.subgraph(graph, node_ids)
			
		pn = {parent_node}
		subgraph_from_leaves = get_subgraph_from_leaves(graph, pn)		
			
		def get_cell_id_wrt_label(graph: nx.DiGraph) -> dict[str, str]:  
			"""Get a dict with cell labels as keys (e.g. "macrophage") and their unique CL id as values ("CL:0000235").""" 
			cell_id_wrt_label = {}
			for id, data in graph.nodes.items():
				cell_id_wrt_label[data["name"]] = id
			return cell_id_wrt_label
	    
		cell_id_wrt_label = get_cell_id_wrt_label(subgraph_from_leaves) 
		
		parent_list = []
		for parent_term in cell_id_wrt_label:
			parent_list.append(cell_id_wrt_label[parent_term])	
			
		if CL_predicted in parent_list:
			LS += 1
			CL_list[i] = parent_node
		
		# Calculate the accuracy of every prediction
		if parent_node == CL_predicted:
			Dmin = 0
		elif CL_predicted in children_list :
			undirected_graph = graph.to_undirected()
			Dmin = nx.shortest_path_length(undirected_graph, source=CL_predicted, target=parent_node)	
			Dmin = Dmin
			accuracy += Dmin
		elif CL_predicted in parent_list :
			undirected_graph = graph.to_undirected()
			Dmin = nx.shortest_path_length(undirected_graph, source=CL_predicted, target=parent_node)	
			Dmin = - Dmin
			accuracy += Dmin
		D_list.append(Dmin)
			

			
	if tool == "CellO":
		Dmin_CellO = D_list
		accuracy_CellO = 1 / (accuracy / nb_CL)
		CellO_CL_list = CL_list
	else:
		Dmin_scMRMA = D_list
		accuracy_scMRMA = 1 / (accuracy / nb_CL)
		scMRMA_CL_list = CL_list
		
		
	dict_comp_by_tool[tool]["Same_Ontology_term"] = consistency		
	dict_comp_by_tool[tool]["More_specific_term"] = MS	
	dict_comp_by_tool[tool]["Less_specific_term"] = LS	
	dict_comp_by_tool[tool]["Not_consistent"] = (len(CL_list_CellO)-consistency-LS-MS)			
	consistency_metric[tool] = round(((consistency + MS + LS)/nb_CL), 3)	


Global_metric_CellO = (consistency_metric["CellO"] ** 2) * accuracy_CellO
Global_metric_scMRMA = (consistency_metric["scMRMA"] ** 2) * accuracy_scMRMA
Global_metric_list = [Global_metric_CellO, Global_metric_scMRMA]

Same_Ontology_term_list = []
More_specific_term_list = []
Less_specific_term_list = []
Not_consistent_list = []
consistency_metric_list = []

for tool in dict_comp_by_tool:
	Same_Ontology_term_list.append(dict_comp_by_tool[tool]["Same_Ontology_term"])
	More_specific_term_list.append(dict_comp_by_tool[tool]["More_specific_term"])
	Less_specific_term_list.append(dict_comp_by_tool[tool]["Less_specific_term"])
	Not_consistent_list.append(dict_comp_by_tool[tool]["Not_consistent"])
	consistency_metric_list.append(consistency_metric[tool])
accuracy_metric_list = [accuracy_CellO, accuracy_scMRMA]

# Creating dataframes with ids and CL
df_tool_AC = pd.DataFrame(list(zip(Same_Ontology_term_list, More_specific_term_list, Less_specific_term_list, Not_consistent_list, consistency_metric_list, accuracy_metric_list, Global_metric_list)), index = list_tools, columns = ['Same Ontology term', 'More specific term', 'Less specific term', 'Not consistent', 'Consistency', 'Accuracy', 'Global metric'])
print(df_tool_AC)

### Figures

# Import the library
import matplotlib.pyplot as plt
from matplotlib_venn import venn2, venn3, venn3_circles

# First Venn D CellO
a = int(dict_comp_by_tool["CellO"]["Not_consistent"])
b = int(dict_comp_by_tool["CellO"]["Not_consistent"])
c = dict_comp_by_tool["CellO"]["Same_Ontology_term"] + dict_comp_by_tool["CellO"]["More_specific_term"] + dict_comp_by_tool["CellO"]["Less_specific_term"]
venn2(subsets = (a, b, c), set_labels = ('CellO', 'Annotated Cells'))
plt.title('Venn Diagram Consistency')
plt.savefig('../RESULTS/Venn_Diagram_CellO.png')
plt.show()

# First Venn D scMRMA
a = int(dict_comp_by_tool["scMRMA"]["Not_consistent"])
b = int(dict_comp_by_tool["scMRMA"]["Not_consistent"])
c = dict_comp_by_tool["scMRMA"]["Same_Ontology_term"] + dict_comp_by_tool["scMRMA"]["More_specific_term"] + dict_comp_by_tool["scMRMA"]["Less_specific_term"]
venn2(subsets = (a, b, c), set_labels = ('scMRMA', 'Annotated Cells'))
plt.title('Venn Diagram Consistency')
plt.savefig('../RESULTS/Venn_Diagram_scMRMA.png')
plt.show()




# Metrics 
import plotly.graph_objects as go

# Consistency
tools=['CellO', 'scMRMA']

fig = go.Figure(data=[
    go.Bar(name='Consistency', x=tools, y=consistency_metric_list, text = consistency_metric_list)
])
fig.update_layout(yaxis_range=[0,1], title_text='Consistency between tools and annotated cells results', yaxis_title="Consistency")
fig.update_layout(barmode='group')
os.chdir("../")
fig.write_image("RESULTS/Consistency.jpeg")

# Accuracy
std_CellO = statistics.pstdev(Dmin_CellO)
std_scMRMA = statistics.pstdev(Dmin_scMRMA)
tools=['CellO', 'scMRMA']

fig = go.Figure()
fig.add_trace(go.Bar(
    name='Control',
    x=['CellO', 'scMRMA'], y=[accuracy_metric_list[0], accuracy_metric_list[1]], marker_color='lightsalmon', text = [accuracy_metric_list[0],   accuracy_metric_list[1]],
    error_y=dict(type='data', array=[std_CellO, std_scMRMA])
    ))
fig.update_layout(title_text='Accuracy between tools and annotated cells results', yaxis_title="Accuracy")
fig.update_layout(barmode='group')
fig.write_image("RESULTS/Accuracy.jpeg")

# Global metric
fig = go.Figure(data=[
    go.Bar(name='Global metric', x=tools, y=Global_metric_list, marker_color='indianred', text = Global_metric_list)
])
fig.update_layout(title_text='Global metric between tools and annotated cells results', yaxis_title="Global metric")
fig.write_image("RESULTS/Global_metric.jpeg")


# Tools VS Annotated Cells 

import plotly.graph_objects as go
tools=['CellO', 'scMRMA', 'Annotated Cells']

fig = go.Figure(data=[
    go.Bar(name='Same Ontology term', x=tools, y=Same_Ontology_term_list, text = Same_Ontology_term_list),
    go.Bar(name='More specific term', x=tools, y=More_specific_term_list, text = More_specific_term_list),
    go.Bar(name='Less specific term', x=tools, y=Less_specific_term_list, text = Less_specific_term_list)
])
fig.update_layout(barmode='stack', title_text='Tools VS Annotated Cells', yaxis_title="Ontology terms")
fig.write_image("RESULTS/Tool_VS_Annotated_Cells.jpeg")


print("----------------------------------------------------------------------------------------------------------------------------------------------------------------------")
print("|     CELLO VS scMRMA     |")
print("---------------------------", "\n")

print(df2, "\n")

### Calculate metrics
consistency = 0
for i in range (len(CL_list_CellO)):
	CellO_CL = CL_list_CellO[i]
	scMRMA_CL = CL_list_scMRMA[i]
	if CellO_CL == scMRMA_CL:
		consistency += 1
		
mean = round((consistency / len(CL_list_CellO)) * 100, 2)		

TOOLS = {
"CellO": CL_list_CellO,
"scMRMA": CL_list_scMRMA
}


# Evaluate the consistency between CellO and scMRMA
print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
print("| Evaluate the consistency between CellO and scMRMA |")
print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>", "\n")
print(" Consistency : ", consistency, "/", len(CL_list_CellO), " ==>", mean, "%", "\n")

# Do a tool find CL terms more specific that the other tool can't find ?
print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
print("| Do a tool find CL terms more or less specific that the other tool can't find ? |")
print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>", "\n")

list_tools = ("CellO", "scMRMA") 
dict_comp = defaultdict(defaultdict)
consistency_metric = defaultdict(int)	
	
for j in range(len(list_tools)-1):
	tool = list_tools[j]
	tool_for_comparison = list_tools[-1] 
	CL1_list = TOOLS[tool]
	CL2_list = TOOLS[tool_for_comparison]
	LS = -consistency
	MS = -consistency
	for i in range (len(CL_list_CellO)):
		CL_M1 = CL1_list[i]
		CL_M2 = CL2_list[i]
		if type(CL_M2) == set :
			CL_M2 = list(CL_M2)[0]
			
		# Do a tool find CL terms more specific that the other tool can't find ?
		objective = "Most_specific"
		def get_subgraph_from_root(graph: nx.DiGraph, root_id: str) -> nx.DiGraph:

			"""From a broad root term, generate a subgraph containing all the children terms.""" 
			node_ids = nx.ancestors(graph, root_id)
			node_ids.add(root_id)
			return nx.subgraph(graph, node_ids)
			
		subgraph_from_root = get_subgraph_from_root(graph, CL_M2)
		
			
		def get_cell_id_wrt_label(graph: nx.DiGraph) -> dict[str, str]:  
			"""Get a dict with cell labels as keys (e.g. "macrophage") and their unique CL id as values ("CL:0000235").""" 
			cell_id_wrt_label = {}
			for id, data in graph.nodes.items():
				cell_id_wrt_label[data["name"]] = id
			return cell_id_wrt_label
	    
		cell_id_wrt_label = get_cell_id_wrt_label(subgraph_from_root) 
		
		children_list = []
		for children_term in cell_id_wrt_label:
			children_list.append(cell_id_wrt_label[children_term])
		
			
		if CL_M1 in children_list:
			MS += 1
		
		# Do a tool find CL terms less specific that the other tool can't find ?
		objective = "Less_specific"
		def get_subgraph_from_leaves(
			graph: nx.DiGraph, leaf_ids: list[str] | set[str]
		) -> nx.DiGraph:
			"""Generate a subgraph encompassing all the input node ids and their parents from a networkx Graph."""
			node_ids = set()
			for lid in leaf_ids:
				node_ids.update(nx.descendants(graph, lid))
				node_ids.add(lid)
			return nx.subgraph(graph, node_ids)
			
		CL_M2 = {CL_M2}
		subgraph_from_leaves = get_subgraph_from_leaves(graph, CL_M2)		
			
		def get_cell_id_wrt_label(graph: nx.DiGraph) -> dict[str, str]:  
			"""Get a dict with cell labels as keys (e.g. "macrophage") and their unique CL id as values ("CL:0000235").""" 
			cell_id_wrt_label = {}
			for id, data in graph.nodes.items():
				cell_id_wrt_label[data["name"]] = id
			return cell_id_wrt_label
	    
		cell_id_wrt_label = get_cell_id_wrt_label(subgraph_from_leaves) 
		
		parent_list = []
		for parent_term in cell_id_wrt_label:
			parent_list.append(cell_id_wrt_label[parent_term])
				
		if CL_M1 in parent_list:
			LS += 1
	
	dict_comp[tool]["Same_Ontology_term"] = consistency		
	dict_comp[tool]["More_specific_term"] = MS	
	dict_comp[tool]["Less_specific_term"] = LS	
	dict_comp[tool]["Not_consistent"] = (nb_CL -consistency -LS -MS)			
	consistency_metric[tool] = round(((consistency + MS + LS)/nb_CL), 3)

print(consistency)
print(MS)
print(LS)		
		
Same_Ontology_term_list = []
More_specific_term_list = []
Less_specific_term_list = []
Not_consistent_list = []
consistency_metric_list = []
for tool in dict_comp:
	Same_Ontology_term_list.append(dict_comp[tool]["Same_Ontology_term"])
	More_specific_term_list.append(dict_comp[tool]["More_specific_term"])
	Less_specific_term_list.append(dict_comp[tool]["Less_specific_term"])
	Not_consistent_list.append(dict_comp[tool]["Not_consistent"])
	consistency_metric_list.append(consistency_metric[tool])

# Creating dataframes with ids and CL
df_tool_AC = pd.DataFrame(list(zip(Same_Ontology_term_list, More_specific_term_list, Less_specific_term_list, Not_consistent_list, consistency_metric_list)), index = list_tools, columns = ['Same Ontology term', 'More specific term', 'Less specific term', 'Not consistent', 'Consistency'])
print(df_tool_AC)


### Figure 

# Tool VS Tool
import plotly.graph_objects as go
tools=['CellO', 'scMRMA', 'Annotated Cells']

fig = go.Figure(data=[
    go.Bar(name='Same Ontology term', x=tools, y=Same_Ontology_term_list, text = Same_Ontology_term_list),
    go.Bar(name='More specific term', x=tools, y=More_specific_term_list, text = More_specific_term_list),
    go.Bar(name='Less specific term', x=tools, y=Less_specific_term_list, text = Less_specific_term_list)
])
fig.update_layout(barmode='stack', title_text='CellO VS scMRMA', yaxis_title="Ontology terms")
fig.write_image("RESULTS/CellO_VS_scMRMA.jpeg")

# Consistency

tools=['CellO/scMRMA']
y = [consistency_metric_list[0]]
fig = go.Figure(data=[
    go.Bar(name='Consistency', x=tools, y=y),

])
fig.update_layout(barmode='stack', title_text='Consistency between CellO and scMRMA')
fig.update_layout(yaxis_range=[0,1], yaxis_title="Consistency")
fig.write_image("RESULTS/Consistency_CellO_scMRMA.jpeg")

# First Venn D scMRMA VS CellO
a = int(dict_comp["CellO"]["Not_consistent"])
b = a
c = dict_comp["CellO"]["Same_Ontology_term"] + dict_comp["CellO"]["More_specific_term"] + dict_comp["CellO"]["Less_specific_term"]
venn2(subsets = (a, b, c), set_labels = ('scMRMA', 'CellO'))
plt.title('Venn Diagram Consistency')
plt.savefig('RESULTS/Venn_Diagram_scMRMA_CellO.png')
plt.show()

# Global Venn Diagram

a = 0
b = 0
d = 0 
e = 0
f = 0
g = 0
for i in range(len(CL_list_CT)):
	if CellO_CL_list[i] == CL_list_CT[i] and CellO_CL_list[i] == scMRMA_CL_list[i]:
		g += 1
	if CellO_CL_list[i] == CL_list_CT[i] and CellO_CL_list[i] != scMRMA_CL_list[i]:
		e += 1
	if scMRMA_CL_list[i] == CL_list_CT[i] and scMRMA_CL_list[i] != CellO_CL_list[i]:
		f += 1
c = c -g	
a = nb_CL -e -g -c
b = nb_CL -c -g -f
d = nb_CL -e -g -f

v=venn3(subsets = (a, b, c, d,e,f,g), set_labels = ('CellO', 'scMRMA', 'Annotated cells'))
c=venn3_circles(subsets = (a, b, c, d,e,f,g), linestyle='dashed', linewidth=1, color="grey")
plt.title('Venn Diagram consistency')
plt.savefig('RESULTS/Venn_Diagram_consistency.png')
plt.show()



